import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '/bindings',
      name: 'bindings',
      component: () => import('./views/Bindings.vue'),
    },
    {
      path: '/conditional',
      name: 'conditional',
      component: () => import('./views/ConditionalRendering.vue'),
    },
    {
      path: '/list',
      name: 'list',
      component: () => import('./views/ListRendering.vue'),
    },
    {
      path: '/img',
      name: 'image',
      component: () => import('./views/ImageRendering.vue'),
    },
    {
      path: '/filters',
      name: 'filters',
      component: () => import('./views/Filters.vue'),
    },
    {
      path: '/test',
      name: 'test',
      component: () => import('./views/Test.vue'),
    },
  ],
});
